// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FunnyFishing.h"
#include "FunnyFishingGameMode.h"
#include "FunnyFishingHUD.h"
#include "FunnyFishingCharacter.h"
#include "Blueprint/UserWidget.h"

void AFunnyFishingGameMode::BeginPlay()
{
	Super::BeginPlay();
	AFunnyFishingCharacter * MyCharacter = Cast<AFunnyFishingCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (PlayerHUDClass != nullptr) {
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);
		if (CurrentWidget != nullptr) {
			CurrentWidget->AddToViewport();
		}
	}
}

AFunnyFishingGameMode::AFunnyFishingGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFunnyFishingHUD::StaticClass();
}
