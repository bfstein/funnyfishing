// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "SimpleFishCharacter.generated.h"

UCLASS()
class FUNNYFISHING_API ASimpleFishCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASimpleFishCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    //health
    UPROPERTY(EditAnywhere, Category = FishMechanics)
    float Health = 2.0f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    float TakeDamage(float Damage, FDamageEvent const& DamageEvent,AController* EventInstigator, AActor* DamageCauser);
    
    class AFunnyFishingCharacter* Player;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    class UParticleSystem* DeathFX;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    class UParticleSystem* ShootFX;
    
    UPROPERTY(Transient)
    class UParticleSystemComponent* FireFX;
    
    UParticleSystemComponent* PlayDeathFX(UParticleSystem* Effects);
    
    void EndLife();

};
