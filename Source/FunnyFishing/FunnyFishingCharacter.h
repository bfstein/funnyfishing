// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "FunnyFishingCharacter.generated.h"

UENUM()
enum GunType
{
    Shotgun,
    Machinegun
};

class UInputComponent;

UCLASS(config=Game)
class AFunnyFishingCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

public:
	AFunnyFishingCharacter();
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		int Score=0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		FString ScoreString;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		float HP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		float MaxHP=100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		float Bullets;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		float MaxBullets=10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		FString GameOverString;
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AFunnyFishingProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

	//Muzzle FX
	// Muzzle_FX
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	class UParticleSystem* MuzzleFX;
	UPROPERTY(Transient)
	class UParticleSystemComponent* MSC;
	void Jump() override;
	bool isOnBoat = false;
protected:
	
	/** Fires a projectile. */
	void OnFire();
    void OnStopFire();
	/**counter for muzzle_FX */
	float counter;
	float counterEnd;
	float deactivationStep = 0.8;
	bool gunActive;

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }
    
    
    void GetInShip(class ABoat* s);
    ABoat* ship;
    
    void Dynamite();

    void Vehicle();
    bool inVehicle;
    
    UPROPERTY(EditAnywhere, Category = Collision)
    float CapsuleRadius = 0.0f;
    
    UPROPERTY(EditAnywhere, Category = Collision)
    float CapsuleHalfHight = 0.0f;


protected:
    
    UPROPERTY(EditAnywhere, Category = ShootingMechanics)
    TEnumAsByte<GunType> ActiveGun = Shotgun;
    
    void ToggleWeapon();
    
    //shooting mechanics
    void ShotgunTrace();
    void MachinegunTrace();
    void WeaponTraceDidHit(FVector StartPos, FVector EndPos, FCollisionQueryParams TraceParams);
    
    UPROPERTY(EditAnywhere, Category = ShootingMechanics)
    float WeaponRange = 5000.0f;
    
    UPROPERTY(EditAnywhere, Category = ShootingMechanics)
    float Damage = 2.0f;
    
    UPROPERTY(EditAnywhere, Category = ShootingMechanics)
    float Spread = 130.0f;
    
    bool IsShooting = false;
    
    bool Underwater = false; //assumes player starts above water
    
public:
    void LaunchPlayer(FVector Direction, float Magnitude);
    
    bool IsUnderwater() { return Underwater; }
    
};

