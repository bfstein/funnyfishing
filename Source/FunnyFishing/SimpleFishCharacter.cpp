// Fill out your copyright notice in the Description page of Project Settings.

#include "FunnyFishing.h"
#include "SimpleFishCharacter.h"
#include "SimpleFishAIController.h"
#include "FunnyFishingCharacter.h"


// Sets default values
ASimpleFishCharacter::ASimpleFishCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    AIControllerClass = ASimpleFishAIController::StaticClass();

}

// Called when the game starts or when spawned
void ASimpleFishCharacter::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast<AFunnyFishingCharacter>(UGameplayStatics::GetPlayerCharacter(this,0));
}

// Called every frame
void ASimpleFishCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASimpleFishCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ASimpleFishCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent,AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        Health -= ActualDamage;
        if (Health <= 0.0f)
        {
            Player->Score++;
            
            // We're dead, don't allow further damage
            bCanBeDamaged = false;
            // TODO: Process death
            //StopAttack();
            FTimerHandle DeathTimer;
            //float Duration = PlayAnimMontage(DeathAnim);
            //GetWorldTimerManager().SetTimer(DeathTimer, this, &ADwarfCharacter::VoidDestroy, Duration-.025f, false);
            auto FX = PlayDeathFX(DeathFX);
            GetWorldTimerManager().SetTimer(DeathTimer, this, &ASimpleFishCharacter::EndLife, .1f, false);
        }
    }
    return ActualDamage;
}


UParticleSystemComponent* ASimpleFishCharacter::PlayDeathFX(UParticleSystem* Effects)
{
    UParticleSystemComponent* PCC = NULL;
    if (Effects)
    {
        PCC = UGameplayStatics::SpawnEmitterAtLocation(this, Effects, this->GetActorLocation());
        //PCC = UGameplayStatics::SpawnEmitterAttached(Effects, GetCapsuleComponent());
        //PCC = UGameplayStatics::SpawnEmitterAttached(Effects,WeaponMesh,TEXT("MuzzleFlashSocket"));
    }
    return PCC;
}


void ASimpleFishCharacter::EndLife()
{
    GetController()->UnPossess();
    Destroy();
}






