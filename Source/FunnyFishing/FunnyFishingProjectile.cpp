// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FunnyFishing.h"
#include "FunnyFishingProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "SimpleFishCharacter.h"
#include "SimpleFishAIController.h"

#include <iostream>
AFunnyFishingProjectile::AFunnyFishingProjectile()
{
    // Use a sphere as a simple collision representation
    CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
    CollisionComp->InitSphereRadius(5.0f);
    CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
    
    // Players can't walk on it
    CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
    CollisionComp->CanCharacterStepUpOn = ECB_No;
    
    // Set as root component
    RootComponent = CollisionComp;
    
    // Use a ProjectileMovementComponent to govern this projectile's movement
    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
    ProjectileMovement->UpdatedComponent = CollisionComp;
    /*ProjectileMovement->InitialSpeed = Speed;
     ProjectileMovement->MaxSpeed = Speed;
     ProjectileMovement->bRotationFollowsVelocity = true;
     ProjectileMovement->bShouldBounce = false;*/
    
    // Die after 3 seconds by default
}
void AFunnyFishingProjectile::BeginPlay()
{
    Super::BeginPlay();
    //ProjectileMovement->InitialSpeed = Speed;
    //ProjectileMovement->MaxSpeed = Speed;
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->bShouldBounce = false;
    GetWorldTimerManager().SetTimer(Handle, this, &AFunnyFishingProjectile::OnExplode, MaxFuzeTime, false);
    
}

void AFunnyFishingProjectile::OnExplode()
{
    TArray<FHitResult> HitResults;
    FVector EndLocation = GetActorLocation();
    
    FCollisionShape CollisionShape;
    CollisionShape.ShapeType = ECollisionShape::Sphere;
    CollisionShape.SetSphere(SphereRadius);
    GetWorld()->SweepMultiByChannel(HitResults, GetActorLocation(), EndLocation, FQuat::Identity, ECollisionChannel::ECC_WorldDynamic, CollisionShape);
    for (auto It : HitResults)
    {
        AActor* CurrentActor = It.GetActor();
        if(CurrentActor != NULL && CurrentActor->IsA(ASimpleFishCharacter::StaticClass()))
        {
            auto Fish = static_cast<ASimpleFishCharacter*>(CurrentActor);
            Fish->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
        }
        
    }
    FTimerHandle DeathTimer;
    //float Duration = PlayAnimMontage(DeathAnim);
    //GetWorldTimerManager().SetTimer(DeathTimer, this, &ADwarfCharacter::VoidDestroy, Duration-.025f, false);
    ProjectileMovement->StopMovementImmediately();

    SetActorHiddenInGame( true );
    
     FXSmoke = PlayDeathFX(SmokeFX);
     FXFire = PlayDeathFX(FireFX);
    
    GetWorldTimerManager().SetTimer(DeathTimer, this, &AFunnyFishingProjectile::EndLife, DeathFuzeTime, false);
}



UParticleSystemComponent* AFunnyFishingProjectile::PlayDeathFX(UParticleSystem* Effects)
{
    UParticleSystemComponent* PCC = NULL;
    if (Effects)
    {
        PCC = UGameplayStatics::SpawnEmitterAtLocation(this, Effects, this->GetActorLocation());
        PCC->CustomTimeDilation = AnimeSpeed;
        //PCC = UGameplayStatics::SpawnEmitterAttached(Effects, GetCapsuleComponent());
        //PCC = UGameplayStatics::SpawnEmitterAttached(Effects,WeaponMesh,TEXT("MuzzleFlashSocket"));
    }
    return PCC;
}

void AFunnyFishingProjectile::EndLife()
{
    FXSmoke->DeactivateSystem();
    FXFire->DeactivateSystem();
    
    Destroy();
}


