// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FunnyFishing.h"
#include "FunnyFishingCharacter.h"
#include "FunnyFishingProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include <string>
#include "SimpleFishCharacter.h"
#include "Boat.h"
#include "BoatController.h"
#include "EngineUtils.h"


#include <iostream>
DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFunnyFishingCharacter

AFunnyFishingCharacter::AFunnyFishingCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	//HUD values
	HP = MaxHP;
	Bullets = MaxBullets;


	counter = 0;
    
    inVehicle = false;
}

void AFunnyFishingCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	std::string tempString = "Score: " + std::to_string(Score);
	ScoreString = tempString.c_str();


	if (HP < 1.0) {//dead
		tempString = "Game Over";
		GameOverString = tempString.c_str();
		APlayerController* PC = Cast<APlayerController>(GetController());
		if (PC)
		{
			PC->SetCinematicMode(true, true, true);
		}
		GetMesh()->Deactivate();
	}
	counter += DeltaTime;
	if (gunActive)
	{
		if (counter > counterEnd)
		{
			MSC->DeactivateSystem();
			gunActive = false;
		}
	}
    
    //Check if player is underwater
    for ( TObjectIterator<AActor> Itr; Itr; ++Itr )
    {
        //BP_OceanWater_21
        
        // Access the subclass instance with the * or -> operators.
        AActor* Guy = *Itr;
        
        if (Guy)
        {
            if (Guy->GetName() == FString("BP_OceanWater_21"))
            {
                FVector Loc = Guy->GetActorLocation();
                if (this->GetActorLocation().Z <= (Loc.Z+35.0f))
                {
                    Underwater = true;
                }
                else
                {
                    Underwater = false;
                }
            }
        }
    }
}

void AFunnyFishingCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;
	UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetMovementComponent()->Deactivate();
	UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->bIgnoresOriginShifting = true;
	Cast<AController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->SetIgnoreLookInput(true);
	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFunnyFishingCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFunnyFishingCharacter::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFunnyFishingCharacter::OnFire);
        PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFunnyFishingCharacter::OnStopFire);
	}

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFunnyFishingCharacter::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFunnyFishingCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFunnyFishingCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFunnyFishingCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFunnyFishingCharacter::LookUpAtRate);
    
    PlayerInputComponent->BindAction("Vehicle", IE_Pressed, this, &AFunnyFishingCharacter::Vehicle);
    
    PlayerInputComponent->BindAction("ToggleWeapon", IE_Pressed, this, &AFunnyFishingCharacter::ToggleWeapon);

    PlayerInputComponent->BindAction("Dynamite", IE_Pressed, this, &AFunnyFishingCharacter::Dynamite);

}

void AFunnyFishingCharacter::OnFire()
{
    IsShooting = true;
	if (HP < 1.0)return;//dead people can't shoot
    
    switch(ActiveGun)
    {
        case Shotgun:
            ShotgunTrace();
            break;
        case Machinegun:
            MachinegunTrace();
            break;
    }
    
    Bullets--;
    
    /*
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<AFunnyFishingProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<AFunnyFishingProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
			Bullets--;
		}
	}
     */

//	// try and play the sound if specified
//	if (FireSound != NULL)
//	{
//		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
//	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}

	if (MuzzleFX != NULL)
	{
		if (!gunActive)
		{
			counterEnd = counter + deactivationStep;
			MSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, FP_Gun, TEXT("MuzzleFlashSocket"));
			gunActive = true;
		}
		
	}
}

void AFunnyFishingCharacter::OnStopFire()
{
    IsShooting = false;
}

void AFunnyFishingCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFunnyFishingCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFunnyFishingCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void AFunnyFishingCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void AFunnyFishingCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
    //std::cout << "AFunnyFishingCharacter::MoveForward" << std::endl;

        if (inVehicle && ship != NULL) {
            std::cout << "ship is not null" << std::endl;
            //ship->controller->MoveForward(Value);
            ship->MoveForward(Value);

            return;
        }
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
        //std::cout << "player pos: x = " << GetActorLocation().X<<"y = " << GetActorLocation().Y<< "z = " << GetActorLocation().Z << std::endl;

	}
}

void AFunnyFishingCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
  //  std::cout << "AFunnyFishingCharacter::MoveRight" << std::endl;
        
        if (inVehicle && ship != NULL) {
            std::cout << "ship is not null" << std::endl;
            //ship->controller->MoveRight(Value);
            ship->MoveRight(Value);
            return;
        }
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFunnyFishingCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFunnyFishingCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}
void AFunnyFishingCharacter::Jump() {
	if (HP < 1.0) {//is dead
		return;
	}
	if (isOnBoat) {//can't jump on a boat
		return;
	}
	Super::Jump();
}
bool AFunnyFishingCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFunnyFishingCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AFunnyFishingCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AFunnyFishingCharacter::TouchUpdate);
	}
	return bResult;
}

void AFunnyFishingCharacter::ToggleWeapon()
{
    switch(ActiveGun)
    {
        case Shotgun:
            ActiveGun = Machinegun;
            break;
        case Machinegun:
            ActiveGun = Shotgun;
            break;
    }
}

void AFunnyFishingCharacter::ShotgunTrace()
{
    static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
    static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));
    // Start from the muzzle's position
    //FVector StartPos = WeaponMesh->GetSocketLocation(MuzzleSocket);
    //FVector StartPos = FP_Gun->GetComponentLocation();
    FVector StartPos = this->GetActorLocation();
    //Get Forward Vector of Camera
    UCameraComponent* PCam = GetFirstPersonCameraComponent();
    FVector Forward = PCam->GetForwardVector();
    // Calculate end position
    FVector EndPos = StartPos + Forward * WeaponRange;
    // Perform trace to retrieve hit info
    FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
    TraceParams.bTraceAsyncScene = true;
    TraceParams.bReturnPhysicalMaterial = true;
    
    //add debug
    const FName TraceTag("MyTraceTag");
    GetWorld()->DebugDrawTraceTag = TraceTag;
    TraceParams.TraceTag = TraceTag;
    
    for ( TObjectIterator<AActor> Itr; Itr; ++Itr )
    {
        //BP_OceanWater_21
        
        // Access the subclass instance with the * or -> operators.
        AActor* Guy = *Itr;
        
        if (Guy)
        {
            if (Guy->GetName() == FString("BP_OceanWater_21"))
            {
                TraceParams.AddIgnoredActor(Guy);
            }
            if (Guy->GetName() == FString("BPBoat_911"))
            {
                TraceParams.AddIgnoredActor(Guy);
            }
        }
    }
    
    //buckshot locations
    FVector EndPos2 = EndPos;
    EndPos2.X += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos2.Y += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos2.Z += FMath::RandRange(Spread * -1.0f, Spread);
    FVector EndPos3 = EndPos;
    EndPos3.X += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos3.Y += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos3.Z += FMath::RandRange(Spread * -1.0f, Spread);
    FVector EndPos4 = EndPos;
    EndPos4.X += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos4.Y += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos4.Z += FMath::RandRange(Spread * -1.0f, Spread);
    FVector EndPos5 = EndPos;
    EndPos5.X += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos5.Y += FMath::RandRange(Spread * -1.0f, Spread);
    EndPos5.Z += FMath::RandRange(Spread * -1.0f, Spread);
    
    TArray<FVector> BuckShot;
    BuckShot.Add(EndPos);
    BuckShot.Add(EndPos2);
    BuckShot.Add(EndPos3);
    BuckShot.Add(EndPos4);
    BuckShot.Add(EndPos5);
    
    // This fires the ray and checks against all objects w/ collision    
    for (auto it : BuckShot)
    {
        WeaponTraceDidHit(StartPos, it, TraceParams);
    }
    
    // try and play the sound if specified
    if (FireSound != NULL)
    {
        UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
    }
}

void AFunnyFishingCharacter::MachinegunTrace()
{
    if (IsShooting)
    {
        FTimerHandle FireTimer;
        GetWorldTimerManager().SetTimer(FireTimer, this, &AFunnyFishingCharacter::MachinegunTrace, .1f, false);
    }
    
    static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
    static FName MuzzleSocket = FName(TEXT("MuzzleFlashSocket"));
    // Start from the muzzle's position
    //FVector StartPos = WeaponMesh->GetSocketLocation(MuzzleSocket);
    //FVector StartPos = FP_Gun->GetComponentLocation();
    FVector StartPos = this->GetActorLocation();
    //Get Forward Vector of Camera
    UCameraComponent* PCam = GetFirstPersonCameraComponent();
    FVector Forward = PCam->GetForwardVector();
    // Calculate end position
    FVector EndPos = StartPos + Forward * WeaponRange;
    // Perform trace to retrieve hit info
    FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
    TraceParams.bTraceAsyncScene = true;
    TraceParams.bReturnPhysicalMaterial = true;
    
    //add debug
    const FName TraceTag("MyTraceTag");
    GetWorld()->DebugDrawTraceTag = TraceTag;
    TraceParams.TraceTag = TraceTag;
    
    for ( TObjectIterator<AActor> Itr; Itr; ++Itr )
    {
        //BP_OceanWater_21
        
        // Access the subclass instance with the * or -> operators.
        AActor* Guy = *Itr;
        
        if (Guy)
        {
            if (Guy->GetName() == FString("BP_OceanWater_21"))
            {
                TraceParams.AddIgnoredActor(Guy);
            }
            if (Guy->GetName() == FString("BPBoat_911"))
            {
                TraceParams.AddIgnoredActor(Guy);
            }
        }
    }
    
    // This fires the ray and checks against all objects w/ collision
    WeaponTraceDidHit(StartPos, EndPos, TraceParams);
    
    // try and play the sound if specified
    if (FireSound != NULL)
    {
        UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
    }
}

void AFunnyFishingCharacter::WeaponTraceDidHit(FVector StartPos, FVector EndPos, FCollisionQueryParams TraceParams)
{
    FHitResult Hit(ForceInit);
    GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos,
                                            FCollisionObjectQueryParams::AllObjects, TraceParams);
    
    // Did this hit anything?
    if (Hit.bBlockingHit)
    {
        ASimpleFishCharacter* Fish = Cast<ASimpleFishCharacter>(Hit.GetActor());
        if (Fish)
        {
            Fish->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
        }
    }
}

void AFunnyFishingCharacter::GetInShip(class ABoat* s)
{
   // auto PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    //PlayerController->UnPossess();
    //PlayerController->Possess(ship);
    //std::cout << "AFunnyFishingCharacter::GetInShip" << std::endl;
    
    ship = s;
    ship->SetPlayer(this);

    //SetActorLocation(ship->GetActorLocation()+ship->offset);
}

void AFunnyFishingCharacter::Vehicle()
{
    std::cout << "IN AFunnyFishingCharacter::Vehicle"<< std::endl;

    if (inVehicle) {
        inVehicle = false;
        //LaunchPlayer(FVector(1.0f,1.0f,1.0f),1000.0f);
    } else {
        TArray<FHitResult> HitResults;
        FVector EndLocation = GetActorLocation();
        
        FCollisionShape CollisionShape;
        //CollisionShape.Capsule = ECollisionShape::Capsule;
        CollisionShape.SetCapsule(CapsuleRadius,CapsuleHalfHight);
        GetWorld()->SweepMultiByChannel(HitResults, GetActorLocation(), EndLocation, FQuat::Identity, ECollisionChannel::ECC_WorldDynamic, CollisionShape);
        std::cout << "IN AFunnyFishingCharacter::Vehicle ELSE"<< std::endl;

        for (auto It : HitResults)
        {
            std::cout << "Collision"<< std::endl;
            AActor* CurrentActor = It.GetActor();
            if(CurrentActor->IsA(ABoat::StaticClass()))
            {
                std::cout << "Collided with a boat"<< std::endl;
                GetInShip(static_cast<ABoat*>(CurrentActor));
                
                inVehicle = true;

            }
        }
    }
}

void AFunnyFishingCharacter::LaunchPlayer(FVector Direction, float Magnitude)
{
    inVehicle = false;
    FVector MDirection = Direction;
    MDirection.Normalize();
    
    MDirection.X *= Magnitude;
    MDirection.Y *= Magnitude;
    MDirection.Z *= Magnitude;
    
    LaunchCharacter(MDirection, true, true);
}

void AFunnyFishingCharacter::Dynamite()
{
    if (ProjectileClass != NULL)
    {
        UWorld* const World = GetWorld();
        if (World != NULL)
        {
            if (bUsingMotionControllers)
            {
                const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
                const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
                World->SpawnActor<class AFunnyFishingProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
            }
            else
            {
                const FRotator SpawnRotation = GetControlRotation();
                // MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
                const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);
                
                //Set Spawn Collision Handling Override
                FActorSpawnParameters ActorSpawnParams;
                ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
                
                // spawn the projectile at the muzzle
                World->SpawnActor<class AFunnyFishingProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
            }
            Bullets--;
        }
    }
}



