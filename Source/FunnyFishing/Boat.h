// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Boat.generated.h"

UCLASS()
class FUNNYFISHING_API ABoat : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABoat();
    

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    void MoveForward(float Value);
    void MoveRight(float Value);
    
    void SetPlayer(APawn* p);
    
    APawn* player;
    
    UPROPERTY(EditAnywhere, Category = Player)
    FVector offset;
    
    UPROPERTY(EditAnywhere, Category = Collision)
    float SphereRadius = 0.0f;
    
    UPROPERTY(EditAnywhere, Category = Movement)
    float forwardSpeed = 0.0f;
    
    UPROPERTY(EditAnywhere, Category = Movement)
    float rotationSpeed = 0.0f;
    
    
    
    class ABoatController* controller;
	
	
};
