// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FunnyFishing : ModuleRules
{
	public FunnyFishing(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG", "AIModule" });

        PrivateDependencyModuleNames.AddRange(new string[] { "Slate","SlateCore" });

    }
}
