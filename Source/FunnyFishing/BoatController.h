// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BoatController.generated.h"

/**
 * 
 */
UCLASS()
class FUNNYFISHING_API ABoatController : public AAIController
{
	GENERATED_BODY()
	
public:

    void MoveForward(float Value);
    void MoveRight(float Value);
    
    void SetPlayer(APawn* p);
    
    APawn* player;
    
    void BeginPlay();

    void Tick(float DeltaTime) override;
    

	
};
