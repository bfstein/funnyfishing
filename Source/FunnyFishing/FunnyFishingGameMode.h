// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "FunnyFishingGameMode.generated.h"

UCLASS(minimalapi)
class AFunnyFishingGameMode : public AGameModeBase
{
	GENERATED_BODY()

		virtual void BeginPlay() override;//override beginplay from base clase
public:
	AFunnyFishingGameMode();
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		TSubclassOf<class UUserWidget> PlayerHUDClass;

	UPROPERTY()
		class UUserWidget* CurrentWidget;

};



