// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "FunnyFishingHUD.generated.h"

UCLASS()
class AFunnyFishingHUD : public AHUD
{
	GENERATED_BODY()

public:
	AFunnyFishingHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	/*Returns the center of the radar as a 2d vector*/
	FVector2D GetRadarCenterPosition();

	void VDrawTile(UTexture2D* tex, float x, float y, float screenX, float screenY, const FColor& TheColor);
	void DrawPlayerInRadar();
	void PerformRadarRaycast();
	void DrawRadar();
protected:
	UPROPERTY(EditAnywhere, Category = Radar)
		UTexture2D* RadarImage;
	/*The start location of our radar*/
	UPROPERTY(EditAnywhere, Category = Radar)
		FVector2D RadarStartLocation = FVector2D(.94f, .095f);

	/*The radius of our radar*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float RadarRadius = 75.0f;

	UPROPERTY(EditAnywhere, Category = Radar)
		float DegreeStep = 0.25f;

	/*The pixel size of the drawable radar actors*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float DrawPixelSize = 5.f;

	/*Sphere height and radius for our raycast*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float SphereHeight = 200.f;

	UPROPERTY(EditAnywhere, Category = Radar)
		float SphereRadius = 5750.f;

	/*Holds a reference to every actor we are currently drawing in our radar*/
	TArray<AActor*> RadarActors;

	/*The distance scale of the radar actors*/
	UPROPERTY(EditAnywhere, Category = Radar)
		float RadarDistanceScale = 100.f;

	/*Converts the given actors' location to local (based on our character)*/
	FVector2D ConvertWorldLocationToLocal(AActor* ActorToPlace);

	/*Draws the raycasted actors in our radar*/
	void DrawRaycastedActors();
};

