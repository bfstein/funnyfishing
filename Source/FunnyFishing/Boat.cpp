// Fill out your copyright notice in the Description page of Project Settings.

#include "FunnyFishing.h"
#include "Boat.h"
#include "FunnyFishingCharacter.h"
#include "BoatController.h"

#include <iostream>



// Sets default values
ABoat::ABoat()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    AIControllerClass = ABoatController::StaticClass();

    //controller;
    
    


}

// Called when the game starts or when spawned
void ABoat::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABoat::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    
    /*TArray<FHitResult> HitResults;
    FVector EndLocation = GetActorLocation();
    
    FCollisionShape CollisionShape;
    CollisionShape.ShapeType = ECollisionShape::Sphere;
    CollisionShape.SetSphere(SphereRadius);
    GetWorld()->SweepMultiByChannel(HitResults, GetActorLocation(), EndLocation, FQuat::Identity, ECollisionChannel::ECC_WorldDynamic, CollisionShape);
    
    for (auto It : HitResults)
    {
        //std::cout << "Collision"<< std::endl;
        AActor* CurrentActor = It.GetActor();
        if(CurrentActor->IsA(AFunnyFishingCharacter::StaticClass()))
        {
            //std::cout << "Collided with a boat"<< std::endl;
            static_cast<AFunnyFishingCharacter*>(CurrentActor)->GetInShip(this);
			static_cast<AFunnyFishingCharacter*>(CurrentActor)->isOnBoat = true;
        }
        
        
    }*/
    
    //AddMovementInput(GetActorForwardVector(), 1);

}

// Called to bind functionality to input
void ABoat::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
    
    
    check(PlayerInputComponent);
    
    PlayerInputComponent->BindAxis("MoveForward", this, &ABoat::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ABoat::MoveRight);


}

void ABoat::MoveForward(float Value)
{
    if (Value != 0.0f && player != NULL)
    {
        // add movement in that direction
        std::cout << "AShip::MoveForward"<< std::endl;
        
        //SetActorLocation(GetActorLocation()+GetActorForwardVector()* Value);
        std::cout << "boat before pos: x = " << GetActorLocation().X<<"y = " << GetActorLocation().Y<< "z = " << GetActorLocation().Z << std::endl;

        AddMovementInput(forwardSpeed*GetActorForwardVector(), Value);
        std::cout << "boat after pos: x = " << GetActorLocation().X<<"y = " << GetActorLocation().Y<< "z = " << GetActorLocation().Z << std::endl;
        player->SetActorLocation(GetActorLocation()+offset);
    }
}

void ABoat::MoveRight(float Value)
{
    if (Value != 0.0f && player != NULL)
    {
        std::cout << "AShip::MoveRight"<< std::endl;
        
        // add movement in that direction
        //SetActorRotation(GetActorRotation()+FRotator(0.0f,Value,0.0f));
        FRotator Rot = FRotator(rotationSpeed*Value*0.0f,
                                rotationSpeed*Value*1.0f,
                                rotationSpeed*Value*0.0f);
        AddActorLocalRotation(Rot);

    }
}
void ABoat::SetPlayer(APawn* p)
{
    player = p;
    player->SetActorLocation(GetActorLocation()+offset);

}

