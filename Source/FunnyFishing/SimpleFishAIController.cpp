// Fill out your copyright notice in the Description page of Project Settings.

#include "FunnyFishing.h"
#include "SimpleFishAIController.h"
#include <iostream>
#include "FunnyFishingCharacter.h"
#include "FunnyFishingProjectile.h"
#include "SimpleFishCharacter.h"

void ASimpleFishAIController::BeginPlay()
{
    Super::BeginPlay();
    //Player = UGameplayStatics::GetPlayerPawn(this, 0);
    OriginPoint = FVector();
    //MyState = Start;
	playerCharacter = Cast<AFunnyFishingCharacter>(UGameplayStatics::GetPlayerCharacter(this,0));
    
    Player = Cast<AFunnyFishingCharacter>(UGameplayStatics::GetPlayerCharacter(this,0));

	counter = 0;
}

void ASimpleFishAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    if (Player && GetPawn())
    {
        float PlayerDist = FVector::Dist(Player->GetActorLocation(), GetPawn()->GetActorLocation());
        
        if (PlayerDist < FishVisionRadius)
        {
            CloseToPlayer = true;
        }
        else
        {
            CloseToPlayer = false;
        }
        
        if (PlayerDist < ShrimpRange)
        {
            VeryCloseToPlayer = true;
        }
        else
        {
            VeryCloseToPlayer = false;
        }
    }
	switch(fishType)
    {
        case huntFish:
            Hunt(DeltaTime);
            break;
        case followPlayerFish:
            FollowPlayer(DeltaTime);
            break;
        case figureEightFish:
            FigureEight(DeltaTime);
            break;
        case shootFish:
            Shoot(DeltaTime);
            break;
		case bossFish:
			BossFishAction(DeltaTime);
			break;
	}

}

void ASimpleFishAIController::BossFishAction(float DeltaTime) {

	if (!Player) { return; }

	FVector PlayerLoc = Player->GetActorLocation();

	APawn* MyPawn = GetPawn();
	if (MyPawn != nullptr)
	{
		switch (fishState)
		{
		case nothing:
			if (stalled(DeltaTime, roamTime)) {
				MovementVec = FVector(((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5) * 2, ((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5) * 2, ((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5)-.3);
				fishState = roam;
			}
			break;
		case roam:
			if (stalled(DeltaTime, roamTime*2)) {
				MovementVec = FVector(((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5) * 2, ((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5) * 2, ((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5)-.3);
			}
			if (FVector::Dist(PlayerLoc, GetPawn()->GetActorLocation()) < 2000){
				MovementVec = PlayerLoc - MyPawn->GetActorLocation();
				fishState = charge;
			}
			break;
		case attack:
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Yellow, TEXT("JING YI!!"));
			UGameplayStatics::SpawnSoundAttached(chomp, RootComponent);
			playerCharacter->HP = playerCharacter->HP - 20;
			fishState = nothing;
			ShrimpFire();
			break;
		case charge:
			if (stalled(DeltaTime, roamTime)) {
				fishState = roam;
			}
			if (FVector::Dist(PlayerLoc, GetPawn()->GetActorLocation()) < 300)
				fishState = attack;
			break;
		}
		MovementVec.Normalize();
		if (fishState == charge)moveScaleValue = 1;
		else moveScaleValue = .2;
		MyPawn->AddMovementInput(MovementVec, moveScaleValue);

	}
}
bool ASimpleFishAIController::stalled(float DeltaTime, float stallTime) {
	counter = counter + DeltaTime;
	if (counter > stallTime) {
		counter = 0;
		return true;
	}
	return false;
}

void ASimpleFishAIController::FollowPlayer(float DeltaTime)
{
    if (!Player) { return; }
    
    FVector PlayerLoc = Player->GetActorLocation();
    
    APawn* MyPawn = GetPawn();
    if (MyPawn != nullptr)
    {
        MovementVec = PlayerLoc - MyPawn->GetActorLocation();
        
        MyPawn->AddMovementInput(FVector(MovementVec.X,
                                         MovementVec.Y,
                                         MovementVec.Z),1.0f);
    }
}


void ASimpleFishAIController::FigureEight(float DeltaTime)
{
    APawn* MyPawn = GetPawn();
    if (MyPawn == nullptr) { return; }
    
    //set origin point equal to spawn point
    if (OriginPoint.Equals(FVector()))
    {
        FVector PawnLoc = MyPawn->GetActorLocation();
        OriginPoint = PawnLoc;
        return;
    }
    
    //move around in a circle
    if (MyPawn != nullptr)
    {
        
        //if close to origin point, flip direction
        //        FVector PawnLoc = MyPawn->GetActorLocation();
        //        if (JankyCounter <= 0 &&
        //            fabs(OriginPoint.X - PawnLoc.X) < 8.0f &&
        //            fabs(OriginPoint.Y - PawnLoc.Y) < 8.0f)
        //        {
        //            CircleRotation *= -1.0f;
        //            //std::cout << "Hello" << std::endl;
        //            OriginPoint = PawnLoc;
        //        }
        
        JankyCircleCounter--;
        if (JankyCircleCounter <= 0)
        {
            CircleRotation *= -1.0f;
            JankyCircleCounter = JankyCircleCounterTime;
        }
        
        FRotator Rot = FRotator(CircleRotation*0.0f,
                                CircleRotation*1.0f,
                                CircleRotation*0.0f);
        MyPawn->AddActorLocalRotation(Rot);
        
        FVector Forward = MyPawn->GetActorForwardVector();
        Forward.Z = .20f*CircleRotation;
        MyPawn->AddMovementInput(Forward,1.0f);
    }
    
    //if player is underwater, go to hunt
    if (Player)
    {
        if (Player->IsUnderwater())
        {
            if (fishType == shootFish)
            {
                return;
            }
            
            PrevType = fishType;
            fishType = huntFish;
        }
    }
}

void ASimpleFishAIController::Hunt(float DeltaTime)
{

	if (!Player) { return; }

	FVector PlayerLoc = Player->GetActorLocation();

	APawn* MyPawn = GetPawn();
	if (MyPawn != nullptr)
	{
		switch (fishState)
        {
            case chase:
                MovementVec = PlayerLoc - MyPawn->GetActorLocation();
                if (FVector::Dist(PlayerLoc, GetPawn()->GetActorLocation()) < 150.0)
                {
                    fishState = attack;
                }
                break;
            case attack:
				UGameplayStatics::SpawnSoundAttached(chomp, RootComponent);
                MovementVec = FVector(((static_cast <float> (rand()) / static_cast <float> (RAND_MAX))-.5)*2, ((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5) * 2, ((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) - .5) * 2);
                playerCharacter->HP = playerCharacter->HP - 10;
                fishState = roam;
                break;
            case roam:
                counter = counter + DeltaTime;
                if (counter> roamTime)
                {
                    fishState = chase;
                    counter = 0;
                }
			break;
		}
		MyPawn->AddMovementInput(FVector(MovementVec.X,
			MovementVec.Y,
			MovementVec.Z), 1.0f);

	}
    
    if (Player)
    {
        if (Player->IsUnderwater() == false)
        {
            fishType = PrevType;
        }
    }

}

void ASimpleFishAIController::Shoot(float DeltaTime)
{
    if (!CloseToPlayer)
    {
        FigureEight(DeltaTime);
    }
    else
    {
        if (!VeryCloseToPlayer)
        {
            FollowPlayer(DeltaTime);
        }
        else
        {
            if (GEngine)
            {
                //GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Yellow, TEXT("Close!"));
                
                ShrimpFire();
                
                //do animation
//                if (PreAttackAnim)
//                {
//                    FTimerHandle AttackTimer;
//                    float Duration = static_cast<ACharacter*>(GetPawn())->PlayAnimMontage(PreAttackAnim);
//                    Duration += .1f;
//                    GetWorldTimerManager().SetTimer(AttackTimer, this, &ASimpleFishAIController::ShrimpFire, Duration, false);
//                }
                //when animation ends, fire projectile
                //ShrimpFire();
            }
        }
    }
}


void ASimpleFishAIController::ShrimpFire()
{
    auto TheFish = static_cast<ASimpleFishCharacter*>(GetPawn());
    
//    if (TheFish->ShootFX)
//    {
//        auto FX = TheFish->PlayDeathFX(TheFish->ShootFX);
//    }
    
    if (Player && GetPawn())
    {
        //when projectile hits, throw player
        FVector Direction = Player->GetActorLocation() - GetPawn()->GetActorLocation();
        Direction.Normalize();
        Direction.Z = 1.0f;
        
        if (!Player->IsUnderwater())
        {
            Player->LaunchPlayer(Direction, LaunchMagnitude);
        }
		if (fishType == bossFish) {
			Player->LaunchPlayer(Direction, LaunchMagnitude);
		}
    }
}





