// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "SimpleFishAIController.generated.h"

/**
 * 
 */
UENUM()
enum FishState {
	roam,
	chase,
	attack,
	dead,

	charge,
	shoot,
	nothing
};
UENUM()
enum FishType {
	followPlayerFish,
	figureEightFish,
	huntFish,
    shootFish,
	bossFish
};

UCLASS()
class FUNNYFISHING_API ASimpleFishAIController : public AAIController
{
	GENERATED_BODY()
	
    void BeginPlay() override;
    
    void Tick(float DeltaTime) override;
    
protected:
    class AFunnyFishingCharacter* Player;
     
    void FollowPlayer(float DeltaTime);
    
    void FigureEight(float DeltaTime);
    
	void Hunt(float DeltaTime);
    
    void Shoot(float DeltaTime);

	void BossFishAction(float DeltaTime);
	//fish type
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
    TEnumAsByte<FishType> fishType=figureEightFish;
    
    TEnumAsByte<FishType> PrevType;
    
    //FigureEightMovement
    FVector OriginPoint;
    float CircleRotation = 1.0f;
    
    int JankyCircleCounterTime = 250;
    int JankyCircleCounter = 250;

	//hunt/ bossFish
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
		TEnumAsByte<FishState> fishState=chase;

	FVector MovementVec;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=fishProperties)
	float roamTime = 2.5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
	float counter;
		TEnumAsByte<FishState> PrevState;
	float moveScaleValue = 1;
	class AFunnyFishingCharacter* playerCharacter;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		class USoundCue* chomp;
	bool stalled(float DeltaTime, float stallTime);
    //shoot
    bool CloseToPlayer = false;
    bool VeryCloseToPlayer = false;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
    float LaunchMagnitude = 1000.0f;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
    float ShrimpRange = 5000.0f;
    
    void ShrimpFire();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
    UAnimMontage* PreAttackAnim;
    
    //detection
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = fishProperties)
    float FishVisionRadius = 1000.0f;
};
