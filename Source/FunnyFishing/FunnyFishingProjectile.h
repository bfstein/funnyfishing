// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Actor.h"
#include "FunnyFishingProjectile.generated.h"

UCLASS(config=Game)
class AFunnyFishingProjectile : public AActor
{
    GENERATED_BODY()
    
    /** Sphere collision component */
    UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
    class USphereComponent* CollisionComp;
    
    /** Projectile movement component */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
    class UProjectileMovementComponent* ProjectileMovement;
    
protected:
    virtual void BeginPlay();
    
public:
    
    AFunnyFishingProjectile();
    void OnExplode();
    void EndLife();
    UParticleSystemComponent* PlayDeathFX(UParticleSystem* Effects);

    /** called when projectile hits something */
    
    /** Returns CollisionComp subobject **/
    FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
    /** Returns ProjectileMovement subobject **/
    FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
    
    
    FTimerHandle Handle;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Damage)
    float Damage;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
    float SphereRadius;
    
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    //float Speed;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
    float MaxFuzeTime;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    class UParticleSystem* SmokeFX;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    class UParticleSystem* FireFX;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    float DeathFuzeTime;
    
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    float AnimeSpeed;
    
    UParticleSystemComponent* FXSmoke;
    UParticleSystemComponent* FXFire;
    
        
};

