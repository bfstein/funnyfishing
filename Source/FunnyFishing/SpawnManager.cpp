// Fill out your copyright notice in the Description page of Project Settings.

#include "FunnyFishing.h"
#include "SpawnManager.h"


// Sets default values
ASpawnManager::ASpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnManager::BeginPlay()
{
	Super::BeginPlay();
    
    GetWorldTimerManager().SetTimer(SpawnTimer, this, &ASpawnManager::OnSpawnTimer, SpawnTime, false);
	
}

// Called every frame
void ASpawnManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawnManager::OnSpawnTimer()
{
    int Points = SpawnPoints.Num();
    
    if (Points == 0) { return; }
    
    int Index = FMath::RandRange(0, Points-1);
    FVector pos = SpawnPoints[Index]->GetActorLocation();
    //FRotator rot = SpawnPoints[Index]->GetActorRotation();
    
    float InPitch = FMath::RandRange(0.0f, 360.0f);
    float InYaw = FMath::RandRange(0.0f, 360.0f);
    float InRoll = FMath::RandRange(0.0f, 360.0f);
    
    FRotator rot = FRotator(0.0f, InYaw, 0.0f);
    
    // Spawn an ACharacter of subclass CharacterClass, at specified position and rotation
    ACharacter* Char = GetWorld()->SpawnActor<ACharacter>(CharacterClass, pos, rot);
	if (SpawnTime>.5)
	SpawnTime = SpawnTime*SpawnMultiplier;
    if (Char)
    {
        // Spawn the AI controller for the character
        Char->SpawnDefaultController();
    }
	GetWorldTimerManager().SetTimer(SpawnTimer, this, &ASpawnManager::OnSpawnTimer, SpawnTime, false);

}

