// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FunnyFishing.h"
#include "FunnyFishingHUD.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "SimpleFishCharacter.h"

AFunnyFishingHUD::AFunnyFishingHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshiarTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshiarTexObj.Object;
}


void AFunnyFishingHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition(Center.X-8.0f,Center.Y);

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );

	DrawRadar();
	PerformRadarRaycast();
	DrawRaycastedActors();
	DrawPlayerInRadar();

	RadarActors.Empty();
}
FVector2D AFunnyFishingHUD::GetRadarCenterPosition()
{
	return (Canvas) ? FVector2D(Canvas->SizeX*RadarStartLocation.X, Canvas->SizeY*RadarStartLocation.Y) : FVector2D(0, 0);
}
void AFunnyFishingHUD::VDrawTile(UTexture2D* tex, float x, float y, float screenX, float screenY, const FColor& TheColor)
{
	if (!Canvas) return;
	if (!tex) return;
	//~

	Canvas->SetDrawColor(TheColor);

	//Draw
	Canvas->DrawTile(
		tex, x, y, //z pos
		screenX, //screen width
		screenY,  //screen height
		0, //texture start width
		0, //texture start height
		tex->GetSurfaceWidth(), //texture width from start
		tex->GetSurfaceHeight(), //texture height from start
		BLEND_Translucent
	);
}
void AFunnyFishingHUD::DrawRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();

		//Actual draw
		VDrawTile(RadarImage,RadarCenter.X-RadarRadius+2.5,RadarCenter.Y-RadarRadius+2.5, RadarRadius*2, RadarRadius*2, FColor(255, 255, 255, 120));
		//DrawLine(RadarCenter.X, RadarCenter.Y, RadarCenter.X + fixedX, RadarCenter.Y + fixedY, FLinearColor::Gray, 1.f);
}


void AFunnyFishingHUD::DrawPlayerInRadar()
{
	FVector2D RadarCenter = GetRadarCenterPosition();

	DrawRect(FLinearColor::Blue, RadarCenter.X, RadarCenter.Y, DrawPixelSize, DrawPixelSize);
}

void AFunnyFishingHUD::PerformRadarRaycast()
{
	APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (Player)
	{
		//TArray<FHitResult> HitResults;
		//FVector EndLocation = Player->GetActorLocation();
		//EndLocation.Z += SphereHeight;

		//FCollisionShape CollisionShape;
		//CollisionShape.ShapeType = ECollisionShape::Sphere;
		//CollisionShape.SetSphere(SphereRadius);
		//GetWorld()->SweepMultiByChannel(HitResults, Player->GetActorLocation(), EndLocation, FQuat::Identity, ECollisionChannel::ECC_WorldDynamic, CollisionShape);

		//for (auto It : HitResults)
		//{
		//	AActor* CurrentActor = It.GetActor();
		//	//if it is an actor that is not self, add it to our array
		//	if (CurrentActor) RadarActors.Add(CurrentActor);
		//	
		//}

		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASimpleFishCharacter::StaticClass(), FoundActors);
		for (auto It : FoundActors) {
			if (FVector::Dist(Player->GetActorLocation(), It->GetActorLocation()) < SphereRadius) {
				RadarActors.Add(It);
			}
		}
	}
}

FVector2D AFunnyFishingHUD::ConvertWorldLocationToLocal(AActor* ActorToPlace)
{
	APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (Player && ActorToPlace)
	{
		FVector ActorsLocal3dVector = Player->GetTransform().InverseTransformPosition(ActorToPlace->GetActorLocation());

		ActorsLocal3dVector = FRotator(0.f, -90.f, 0.f).RotateVector(ActorsLocal3dVector);

		ActorsLocal3dVector /= RadarDistanceScale;

		return FVector2D(ActorsLocal3dVector);
	}
	return FVector2D(0, 0);
}
void AFunnyFishingHUD::DrawRaycastedActors()
{
	FVector2D RadarCenter = GetRadarCenterPosition();

	for (auto It : RadarActors)
	{
		FVector2D convertedLocation = ConvertWorldLocationToLocal(It);

		FVector tempVector = FVector(convertedLocation.X, convertedLocation.Y, 0.f);

		tempVector = tempVector.GetClampedToMaxSize2D(RadarRadius - DrawPixelSize);

		convertedLocation.X = tempVector.X;
		convertedLocation.Y = tempVector.Y;

		DrawRect(FLinearColor::Red, RadarCenter.X + convertedLocation.X, RadarCenter.Y + convertedLocation.Y, DrawPixelSize, DrawPixelSize);
	}
}
